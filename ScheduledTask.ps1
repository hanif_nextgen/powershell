﻿<#

This script lists all the scheduled tasks that are not disabled and save them in the H Drive 
You can put it in scheduler and have a history of all of your scheduled tasks 


#>
function Taskscheduler_history
{
  
  $Today= (Get-date -Format ddMMyy-hhmms)

  $Runnig_Tasks   = Get-ScheduledTask | where {$_.state -ne "Running" }  ;
  $Ready_Tasks    = Get-ScheduledTask | where {$_.state -ne "Ready" } 
  $Disabled_Tasks = Get-ScheduledTask | where {$_.state -ne "Disabled" } 

  $newline = [System.Environment]::NewLine ;

  $Runnig_Tasks = "Running Tasks:" + $Runnig_Tasks.Count
  $Ready_Tasks  = "Ready Tasks:" + $Ready_Tasks.Count 
  $Disabled_Tasks = "Disabled Tasks:" + $Disabled_Tasks.Count 

  
  $filename = 'H:\Logs\TaskScheduler\'+$Today+'.txt' ; 

  Out-File -FilePath $filename -InputObject ($Runnig_Tasks + $newline + $Ready_Tasks + $newline +$Disabled_Tasks) 
  Out-File -FilePath $filename -InputObject ($newline + "All the Running and Ready Tasks:") -Append
  
  Get-ScheduledTask | where {$_.state -ne "Disabled" }  | Get-ScheduledTaskInfo  | SELECT Taskname,  LastRuntime,NextRuntime,Taskpath | Sort-Object Taskname | Out-File $filename -Append

  Out-File -FilePath $filename -InputObject "All the Tasks running our of Windows folder:"  -Append

 Get-ScheduledTask  |   Select-String -Pattern "Windows" -NotMatch  | select * | Out-File $filename -Append  


 }
  

