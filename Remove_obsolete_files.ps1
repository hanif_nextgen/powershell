﻿function remove_obsolete_files{ 

$main_path= "C:\logfiles" ; 
$del_files= 0 ;

Get-ChildItem $main_path -Recurse | foreach { if( $_.LastWriteTime -lt (Get-Date).AddDays(-28) ) {$del_files++ ;Remove-Item $_.fullname }}

# Rename-Item $_.fullname -NewName ($_.FullName + '_old') 
 
Write-Host $del_files "Files were deleted " -ForegroundColor Cyan

  
  }

  remove_obsolete_files ; 